#include "dttabwidget.h"


DTTabWidget::DTTabWidget(QWidget * pParent)
{
     setWindowFlag(Qt::FramelessWindowHint);
     //setAttribute(Qt::WA_TranslucentBackground, true);
     setAttribute(Qt::WA_StyledBackground,true);
     setFixedHeight(80);
     m_pLayout = new QHBoxLayout(this);
     setLayout(m_pLayout);
};

DTTabWidget::~DTTabWidget()
{

}

void DTTabWidget::addTabByName(const QString &qsTabName, QIcon qIcon, QString qsObjectName, bool bShowTabName)
{
    QToolButton* toolBtn = new QToolButton(this);
    toolBtn->setText(qsTabName);
    toolBtn->setIcon(qIcon);
    toolBtn->setObjectName(qsObjectName);
    toolBtn->setFixedSize(100,60);
    toolBtn->setToolButtonStyle(Qt::ToolButtonStyle::ToolButtonTextUnderIcon);
    if(!bShowTabName)
    {
        toolBtn->setToolButtonStyle(Qt::ToolButtonStyle::ToolButtonIconOnly); // 只展示Icon
    }
    m_pLayout->addWidget(toolBtn);
    m_pLayout->addSpacing(5);
    m_vecToolBtns.append(toolBtn);
}

QVector<QToolButton*> DTTabWidget::getToolButtonVec()
{
    return m_vecToolBtns;

}
